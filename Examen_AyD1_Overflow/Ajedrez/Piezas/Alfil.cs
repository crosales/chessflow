using System;

namespace Ajedrez{

    public class Alfil : Pieza{
    
        public Alfil(string nombre, Color color,int pActualFila, int pActualColumna)
            :   base(nombre,color,pActualFila,pActualColumna)
        {
        }



        public override bool ValidarMovimiento(int posicionFinalFila, int posicionFinalColumna)
        {
            const int x = 8;

            if ((Math.Abs(posicionFinalFila - PosicionActualFila) == x) && (Math.Abs(posicionFinalColumna - PosicionActualColumna) == x))
                return true;
            else
            Console.Write("El movimiento no es valido, no esta permitido");

            return false;
        }
    }
}
