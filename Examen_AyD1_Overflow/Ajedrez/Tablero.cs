﻿using System;

namespace Ajedrez
{
    public class Tablero
    {
        public static Pieza[,] Piezas;
        private bool _esTableroNuevo;
        public static int[] PiezasComidas;
      

        public Tablero()
        {
            Piezas = new Pieza[9, 9];
            
            _esTableroNuevo = true;
            PiezasComidas = new int[2];
        }

        public Pieza[,] ObtenerTablero()
        {
            if (_esTableroNuevo)
            {
                LlenarTablero();
                _esTableroNuevo = false;
            }
            return Piezas;
        }

        private void LlenarTablero()
        {
            //-----------------------TORRES------------------------
            //Negras
            var nuevaTorre = new Torre("TN1", Color.Negro, 1, 1);
            Piezas[1, 1] = nuevaTorre;

            nuevaTorre = new Torre("TN2", Color.Negro, 1, 8);
            Piezas[1, 8] = nuevaTorre;

            ////Blancas
            nuevaTorre = new Torre("TB1", Color.Blanco, 8, 1);
            Piezas[8, 1] = nuevaTorre;

            nuevaTorre = new Torre("TB2", Color.Blanco, 8, 8);
            Piezas[8, 8] = nuevaTorre;

            //----------------------CABALLOS-----------------------
            //Negros
            var nuevoCaballo = new Caballo("CN1", Color.Negro, 1, 2);
            Piezas[1, 2] = nuevoCaballo;

            nuevoCaballo = new Caballo("CN2", Color.Negro, 1, 7);
            Piezas[1, 7] = nuevoCaballo;

            ////Blancos
            nuevoCaballo = new Caballo("CB1", Color.Blanco, 8, 2);
            Piezas[8, 2] = nuevoCaballo;

            nuevoCaballo = new Caballo("CB2", Color.Blanco, 8, 7);
            Piezas[8, 7] = nuevoCaballo;

            //---------------------ALFILES-----------------------
            //Negros
            var nuevoAlfil = new Alfil("AN1", Color.Negro, 1, 3);
            Piezas[1, 3] = nuevoAlfil;

            nuevoAlfil = new Alfil("AN2", Color.Negro, 1, 6);
            Piezas[1, 6] = nuevoAlfil;

            //Blancos
            nuevoAlfil = new Alfil("AB1", Color.Blanco, 8, 3);
            Piezas[8, 3] = nuevoAlfil;
            nuevoAlfil = new Alfil("AB2", Color.Blanco, 8, 6);
            Piezas[8, 6] = nuevoAlfil;

            //------------------------REINAS-----------------------
            //Negra
            var nuevaReina = new Reina("QN1", Color.Negro, 1, 4);
            Piezas[1, 4] = nuevaReina;

            //Blanca
            nuevaReina = new Reina("QB1", Color.Blanco, 8, 4);
            Piezas[8, 4] = nuevaReina;

            //------------------------REYES-----------------------
            //Negro
            var nuevoRey = new Rey("KN1", Color.Negro, 1, 5);
            Piezas[1, 5] = nuevoRey;

            ////Blanco
            nuevoRey = new Rey("KB1", Color.Blanco, 8, 5);

            Piezas[8, 5] = nuevoRey;

            //----------------------PEONES------------------------
            //Negros        
            var nuevoPeon = new Peon("PN1", Color.Negro, 2, 1, true);
            Piezas[2, 1] = nuevoPeon;

            //Blancos
            nuevoPeon = new Peon("PB1", Color.Blanco, 7, 1, true);
            Piezas[7, 1] = nuevoPeon;

            for (int c = 2; c <= 8; c++)
            {
                //Negros
                nuevoPeon = new Peon("PN" + c, Color.Negro, 2, c, true);
                Piezas[2, c] = nuevoPeon;

                //Blancos
                nuevoPeon = new Peon("PB" + c, Color.Blanco, 7, c, true);
                Piezas[7, c] = nuevoPeon;
            }
        }

        public int ValidarPosicionYColorPieza(int columnaActual, int filaActual, int turno)
        {
            if (Piezas[columnaActual, filaActual] == null)
            {

                return -1;
            }
            if (((turno == 0) && Piezas[columnaActual, filaActual].Color != Color.Blanco) ||
                ((turno == 1) && Piezas[columnaActual, filaActual].Color != Color.Negro))
            {

                return 0;
            }
            return 1;
        }

        public bool BuscarCoordenada(int columnaActual, int filaActual, int columnaFinal, int filaFinal)
        {
            if (Piezas[columnaFinal, filaFinal] == null)
            {
                return MoverEnTablero(columnaActual, filaActual, columnaFinal, filaFinal);
            }

            return Comer(columnaActual, filaActual, columnaFinal, filaFinal);
        }

        public bool Comer(int columnaActual, int filaActual, int columnaFinal, int filaFinal)
        {

            if (Piezas[columnaActual, filaActual] != null)
            {            
                if (Piezas[columnaFinal, filaFinal].Color == Piezas[columnaActual, filaActual].Color)
                {
                    Console.WriteLine("La posicion esta ocupada por una de sus piezas.");
                    return false;
                }
            }

            if (Piezas[columnaActual, filaActual] is Peon)
            {
                if (Piezas[columnaFinal, filaFinal] != null)
                {
                    return MoverEnTablero(columnaActual, filaActual, columnaFinal, filaFinal);
                }
            }

            if (Piezas[columnaActual, filaActual] is Rey)
            {
                if (MoverEnTablero(columnaActual, filaActual, columnaFinal, filaFinal))
                {
                    return true;
                }
            }
            var pieza2 = Piezas[columnaActual, filaActual];
            if (pieza2 != null && pieza2.Color == Color.Blanco)
                PiezasComidas[0]++;
            else
                PiezasComidas[1]++;

            String jugadorActual, jugadorContrario;
            var pieza1 = Piezas[columnaActual, filaActual];
            if (pieza1 != null && pieza1.Color == Color.Blanco)
            {
                jugadorActual = Principal.Jugador[0];
                jugadorContrario = Principal.Jugador[1];
            }
            else
            {
                jugadorActual = Principal.Jugador[1];
                jugadorContrario = Principal.Jugador[0];
            }

            var pieza = Piezas[columnaFinal, filaFinal];
            if (pieza != null)
                Console.WriteLine(jugadorActual + " con su " + Piezas[columnaActual, filaActual].NombrePieza + " se comio un " + pieza.NombrePieza + " de " + jugadorContrario);
            return MoverEnTablero(columnaActual, filaActual, columnaFinal, filaFinal);
        }

        public bool MoverEnTablero(int columnaActual, int filaActual, int columnaFinal, int filaFinal)
        {
            if ((Piezas[columnaActual, filaActual].ValidarMovimiento(columnaFinal, filaFinal)))
            {

                char tipoPieza = Piezas[columnaActual, filaActual].NombrePieza[0];

                if (tipoPieza == 'T')
                {
                    Piezas[columnaFinal, filaFinal] = new Torre(
                        Piezas[columnaActual, filaActual].NombrePieza,
                        Piezas[columnaActual, filaActual].Color, columnaFinal, filaFinal);
                }
                else if (tipoPieza == 'C')
                {
                    Piezas[columnaFinal, filaFinal] =
                        new Caballo(Piezas[columnaActual, filaActual].NombrePieza,
                            Piezas[columnaActual, filaActual].Color, columnaFinal, filaFinal);
                }

                else if (tipoPieza == 'A')
                {
                    Piezas[columnaFinal, filaFinal] =
                        new Alfil(Piezas[columnaActual, filaActual].NombrePieza,
                            Piezas[columnaActual, filaActual].Color, columnaFinal, filaFinal);
                }
                else if (tipoPieza == 'K')
                {
                    Piezas[columnaFinal, filaFinal] =
                        new Rey(Piezas[columnaActual, filaActual].NombrePieza,
                        Piezas[columnaActual, filaActual].Color, columnaFinal, filaFinal);
                }
                else if (tipoPieza == 'Q')
                {
                    Piezas[columnaFinal, filaFinal] =
                        new Reina(Piezas[columnaActual, filaActual].NombrePieza,
                            Piezas[columnaActual, filaActual].Color, columnaFinal, filaFinal);
                }
                else if (tipoPieza == 'P')
                {
                    Piezas[columnaFinal, filaFinal] =
                        new Peon(Piezas[columnaActual, filaActual].NombrePieza,
                        Piezas[columnaActual, filaActual].Color, columnaFinal, filaFinal, true);
                }
                Piezas[columnaActual, filaActual] = null;

                return true;
            }
            return false;
        }

        public static bool PeonPuedeComer(int posicionEnFila, int posicionEnColumna)
        {
            if (Piezas[posicionEnFila, posicionEnColumna] != null)
                return true;

            return false;
        }

        //public int BuscarReyes(int turno)
        //{
        //    int encontroRey = 0;

        //    for (int i = 0; i < 9; i++)
        //    {
        //        for (int j = 0; j < 9; j++)
        //        {
                   
        //            char tipoPieza = Piezas[i, j].NombrePieza[0];

        //            if (tipoPieza == 'K')
        //            {
        //                encontroRey++;
        //            }
        //        }
        //    }

        //    return encontroRey;
        //}

        public int[] BuscarCoordenadasRey(int turno)
        {
            int [] coordenada =new int [2];

            for (int fila = 0; fila <9; fila++)
            {
                for (int columna = 0; columna < 9; columna++)
                {
                    if (Piezas[fila,columna] != null)
                    {
                        char tipoPieza = Piezas[fila, columna].NombrePieza[0];
                        int _turno=0;
                        if (Piezas[fila, columna].Color == Color.Blanco)
                            _turno = 0;
                        else
                            _turno = 1;


                        if (tipoPieza == 'K' && turno==_turno)
                        {
                            coordenada[0] = fila;
                            coordenada[1] = columna;
                        }
                    }
                }
            }

            return coordenada;
        }
    }

}
