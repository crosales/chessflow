﻿using System;

namespace Ajedrez
{
    class Rey:Pieza
    {
        
        public Rey(string nombrePieza, Color color, int pActualFila, int pActualColumna)
            : base(nombrePieza, color, pActualFila, pActualColumna)
        {
        }

        public override bool ValidarMovimiento(int pFinalFinal, int pFinalColumna)
        {
            if ((Math.Abs(pFinalFinal - PosicionActualFila) == 1) && (Math.Abs(pFinalColumna - PosicionActualColumna) == 1) ||
                (Math.Abs(pFinalFinal - PosicionActualFila) == 1) && (Math.Abs(pFinalColumna - PosicionActualColumna) == 0) ||
                (Math.Abs(pFinalFinal - PosicionActualFila) == 0) && (Math.Abs(pFinalColumna - PosicionActualColumna) == 1)
                )
            {
                return true;
            }
            
            return false;
        }
    }

    
}
