﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ajedrez
{
    public class JaqueMate
    {
        int[] ReyBlanco;
        int[] ReyNegro;

        public JaqueMate()
        {
            ReyBlanco = new int[] { 5, 8 };
            ReyNegro = new int[] { 5, 1 };
        }

        public String IsJaque(int[] Reyblanco, int[] Reynegro)
        {
            int jaqueReyNegro = 0;
            int jaqueReyBlanco = 0;
            for (int filas = 1; filas < 9; filas++)
            {
                for (int columnas = 1; columnas < 9; columnas++)
                {
                    if (Tablero.Piezas[columnas, filas] != null)
                    {
                        if (Tablero.Piezas[columnas, filas].Color.Equals(Color.Negro))
                        {
                            if (Tablero.Piezas[columnas, filas].ValidarMovimiento(Reyblanco[0], Reyblanco[1]) && Principal.ValidarCoordenadas(columnas, filas) == 0)
                                jaqueReyBlanco = 1;
                        }
                        if (Tablero.Piezas[columnas, filas].Color.Equals(Color.Blanco))
                            if (Tablero.Piezas[columnas, filas].ValidarMovimiento(Reynegro[0], Reynegro[1]) && Principal.ValidarCoordenadas(columnas, filas) == 0)
                                jaqueReyNegro = 1;
                    }
                }
            }
            if (jaqueReyNegro == jaqueReyBlanco)
            {
                if (jaqueReyNegro == 1)
                    return "Ambos";
                else
                    return "Ninguno";
            }
            else if (jaqueReyNegro > jaqueReyBlanco)
            {
                return "Negro";
            }
            else
                return "Blanco";
        }

        public bool IsJaqueMate(int[] ReyBlanco, int[] ReyNegro)
        {
            int[][] pieza;
            int[] Coordenas = new int[2];
            Array.Copy(ReyBlanco, 0, ReyBlanco, 0, 2);
            Array.Copy(ReyNegro, 0, ReyNegro, 0, 2);
            Pieza[,] tablero = new Pieza[8, 8];
            for (int filas = 0; filas < 8; filas++)
            {
                for (int columnas = 0; columnas < 8; columnas++)
                {
                    tablero[columnas, filas] = Tablero.Piezas[columnas, filas];
                }
            }
            Pieza piezaActual, PiezaNueva;
            for (int filas = 1; filas < 9; filas++)
            {
                for (int columnas = 1; columnas < 9; columnas++)
                {
                    if (Tablero.Piezas[columnas, filas] != null)
                    {
                        if (Tablero.Piezas[columnas, filas].Color.Equals(IsJaque(ReyBlanco, ReyNegro)))
                        {
                            pieza = MovimientosDisponibles(filas, columnas);
                            for (int i = 0; i < pieza[50][0]; i++)
                            {
                                Array.Copy(pieza[i], 0, Coordenas, 0, 2);
                                Coordenas = pieza[i];
                                piezaActual = tablero[columnas, filas];
                                PiezaNueva = tablero[Coordenas[1], Coordenas[0]];
                                tablero[columnas, filas] = null;
                                tablero[Coordenas[1], Coordenas[0]] = piezaActual;

                                if (tablero[columnas, filas].NombrePieza.ToString()[0] == 'R')
                                {
                                    switch (tablero[columnas, filas].Color)
                                    {
                                        case Color.Blanco:
                                            ReyBlanco[0] = filas;
                                            ReyBlanco[1] = columnas;
                                            break;
                                        case Color.Negro:
                                            ReyNegro[0] = filas;
                                            ReyNegro[1] = columnas;
                                            break;
                                    }
                                }
                                
                                if (IsJaque(ReyBlanco, ReyNegro).Equals("Ninguno"))
                                {
                                    return false;
                                }
                                tablero[columnas, filas] = piezaActual;
                                tablero[Coordenas[1], Coordenas[0]] = PiezaNueva;
                            }
                        }
                    }
                }
            }
            return true;
        }

        private int[][] MovimientosDisponibles(int filai, int Columnai)
        {
            int[][] movimiento = new int[51][];

            for (int i = 0; i < movimiento.Length; i++)
            {
                movimiento[i] = new int[2];
            }


            int[] coordenada = { 0, 0 };
            int CoordenadaDisponible = 0;
            for (int fila = 1; fila < 9; fila++)
            {
                for (int columna = 1; columna < 9; columna++)
                {
                    if (Tablero.Piezas[Columnai, filai].ValidarMovimiento(fila, columna) && Principal.ValidarCoordenadas(filai, Columnai) == 0)
                    {
                        coordenada[0] = fila;
                        coordenada[1] = columna;
                        movimiento[CoordenadaDisponible] = coordenada;
                        CoordenadaDisponible++;
                    }
                }
            }
            movimiento[50][0] = CoordenadaDisponible;
            return movimiento;
        }
    }
}

