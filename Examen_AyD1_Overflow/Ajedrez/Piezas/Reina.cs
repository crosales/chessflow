using System;

namespace Ajedrez{

    public class Reina : Pieza {

        public Reina(string nombre, Color color, int pActualFila, int pActualColumna)
            :   base(nombre, color, pActualFila, pActualColumna){

        }

        public override bool ValidarMovimiento(int posicionFinalY, int posicionFinalX) {
            
            var torre = new Torre(NombrePieza, Color, PosicionActualFila, PosicionActualColumna);
            var alfil = new Alfil(NombrePieza, Color, PosicionActualFila, PosicionActualColumna);


            if (torre.ValidarMovimiento(posicionFinalY, posicionFinalX) || alfil.ValidarMovimiento(posicionFinalY, posicionFinalX))
                return true;
            else
            {
                Console.WriteLine("El movimiento no es valido, no esta permitido");
            }
            return false;
        }
	}
}
  
