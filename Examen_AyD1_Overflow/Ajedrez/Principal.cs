﻿using System;

namespace Ajedrez
{
    class Principal
    {
        public static int Turno = 0;
        public static string[] Jugador = new String[2];
        static readonly JaqueMate _JaqueMate = new JaqueMate();
        public static bool ValidandoJaqueMate = false;

        static void Main()
        {
            var tablero = new Tablero();
            int opcion;
            bool nuevoJuego = false;
            do
            {
                Console.Clear();
                ImprimirMenu();

                opcion = Convert.ToInt32(Console.ReadLine());

                switch (opcion)
                {
                    case 1:

                        Console.Write("Nombre jugador 1 (Blanco):  ");
                        Jugador[0] = Console.ReadLine();
                        Console.Write("Nombre jugador 2 (Negro): ");
                        Jugador[1] = Console.ReadLine();

                        Console.WriteLine();
                        Console.WriteLine();
                        Console.Clear();
                        ImprimirMenu();


                        if (nuevoJuego)
                        {
                            tablero = new Tablero();
                        }

                        Pieza[,] tableroParaImpresion = tablero.ObtenerTablero();

                        Console.WriteLine();
                        Console.WriteLine();
                        ImprimirTablero(tableroParaImpresion);

                        int filaActual;
                        int columnaActual;
                        string ColorActual= "";
                        int columnaFinal;
                        int filaFinal;

                        do
                        {
                            if (Turno == 0)
                                ColorActual = "Blanco";
                            else
                                ColorActual = "Negro";

                            Console.Clear();
                            ImprimirMenu();
                                                        
                            Console.WriteLine();
                            Console.WriteLine();
                            ImprimirTablero(tableroParaImpresion);

                            Console.WriteLine(Jugador[Turno] +"( "+ ColorActual  +") es su turno. Escriba las coordenadas de su movimiento.");
                            do
                            {
                                int validacion;
                                do
                                {
                                    do
                                    {
                                        Console.Write("Coordenada Y actual de pieza a mover (fila):    ");
                                        filaActual = Convert.ToInt32(Console.ReadLine());
                                        Console.Write("Coordenada x actual de pieza a mover (columna): ");
                                        
                                        columnaActual = Convert.ToInt32(Console.ReadLine());
                                        validacion = ValidarCoordenadas(filaActual, columnaActual);
                                        if (validacion == -1)
                                        {
                                            nuevoJuego = true;
                                            break; //terminarJuego;
                                        }
                                    }
                                    while (validacion == 2 || validacion == 0);
                                }
                                while (tablero.ValidarPosicionYColorPieza(filaActual, columnaActual, Turno) == 0);

                                if (tablero.ValidarPosicionYColorPieza(filaActual, columnaActual, Turno) == -1)
                                {
                                    Console.WriteLine("La coordenada esta vacia, no hay ninguna pieza en esa posicion.");
                                }

                                if (tablero.ValidarPosicionYColorPieza(filaActual, columnaActual, Turno) == 0)
                                {
                                    Console.WriteLine("La pieza que desea mover no le pertenece.");
                                }

                                do
                                {
                                    Console.Write("Coordenada Y donde movera la pieza (fila):    ");
                                    filaFinal = Convert.ToInt32(Console.ReadLine());
                                    Console.Write("Coordenada X donde movera la pieza (columna): ");
                                    columnaFinal = Convert.ToInt32(Console.ReadLine());
                                    validacion = ValidarCoordenadas(filaFinal, columnaFinal);
                                    if (validacion == -1)
                                        break; //terminarJuego;
                                }
                                while (validacion == 2 || validacion == 0); //validamos la coordenada final este correcta
                            }
                            while (tablero.BuscarCoordenada(filaActual, columnaActual, filaFinal, columnaFinal) == false); //PENDIENTE
                            
                            if (Turno == 0)
                                Turno += 1;
                            else if (Turno == 1)
                                Turno -= 1;

                            ValidandoJaqueMate = true;
                            Console.WriteLine("Jaque para" + _JaqueMate.IsJaque(tablero.BuscarCoordenadasRey(0), tablero.BuscarCoordenadasRey(1)));

                            if (!_JaqueMate.IsJaque(tablero.BuscarCoordenadasRey(0), tablero.BuscarCoordenadasRey(1)).Equals("Ninguno"))
                            {
                                if (_JaqueMate.IsJaqueMate(tablero.BuscarCoordenadasRey(0), tablero.BuscarCoordenadasRey(1)))
                                {
                                    Console.WriteLine("Jaque Mate!!!!");
                                    Console.ReadKey();
                                    nuevoJuego = true;
                                    Turno = 0;
                                    break; //terminarJuego;
                                }
                            }
                            ValidandoJaqueMate = false;

                        } while (((filaActual != -1) && (columnaActual != -1)) || ((filaFinal != -1) && (columnaFinal != -1)));

                        break;

                    case 2:

                        Console.WriteLine("Saliendo del juego. Terminando programa....\n");
                        break;

                    default:

                        Console.WriteLine("Opcion invalida. Intentelo de nuevo..");
                        break;

                }
            }
            while (opcion != 2);
        }

        static void ImprimirTablero(Pieza[,] tableroParaImpresion)
        {
            for (int fila = 0; fila < tableroParaImpresion.GetLength(0); fila++)
            {
                for (int col = 0; col < tableroParaImpresion.GetLength(1); col++)
                {
                    if (tableroParaImpresion[fila,col] != null)
                        Console.Write(" [ " + tableroParaImpresion[fila,col].NombrePieza + " ]");
                else
                    {
                        if (fila == 0 && (col >= 0 || col <= 8))
                            Console.Write("    " + col + "   ");
                    else
                        if (col == 0 && (fila >= 0 || fila <= 8))
                            Console.Write("    " + fila + "   ");
                    else
                        Console.Write(" [     ]");
                    }
                }
               
                Console.WriteLine(" ");
            }

            Console.WriteLine(" ");
        }

        public static int ValidarCoordenadas(int numY, int numX)
        {
            if( (numY>=1 && numY<=8) && (numX>=1 && numX<=8) ){
                return 1; //la coordenada existe
            }
            
            if( (numY == -1) && (numX == -1) ){
                return AbandonarJuego();
            }
            
            Console.WriteLine("La coordenada no existe, no es valida. Intentelo de nuevo.");
            return 0; //la coordenada no existe
        }

        private static int AbandonarJuego(){
            Console.Write("  ¿Seguro que desea abandonar la partida? (presione): \n" +
                                "   1. Salir. \n" +
                                "   2. Continuar. \n" +
                                "   ¿Que desea hacer?: ");
                
            int opcion = Console.Read();

            switch (opcion)
            {

                case 1:
                    {
                        int ganador, perdedor;
                        if (Turno == 0)
                        {
                            perdedor = 0;
                            ganador = 1;
                        }
                        else
                        {
                            perdedor = 1;
                            ganador = 0;
                        }
                        String resultados = " " + Jugador[perdedor] + " se ha retirado del juego. Gana " +
                                            Jugador[ganador];
                        Console.WriteLine(resultados);
                        //Estadisticas.ControlEstadisticas(jugador[perdedor], jugador[ganador], "abandono", 0);
                        Turno = 0;
                        return -1;
                    }
                case 2:
                    {
                        Console.WriteLine("Regresando al juego...");
                        return 2;
                    }
                default:
                    {
                        Console.WriteLine("La opcion no es valida. Intentelo de nuevo.");
                        return AbandonarJuego();
                    }
            }

        }

        private static void ImprimirMenu(){
         Console.Write("\n..::::::::MENU PRINCIPAL::::::::..\n\n\n" +
                                    "1. Jugar Ajedrez.\n" +
                                    "2. Salir.\n\n\n" +
                                    "Escriba el numero correspondiente a la opcion que desea realizar: ");
        }
    }

}
