﻿
namespace Ajedrez
{
    public abstract class Pieza
    {

        public string NombrePieza;
        public Color Color;
        protected int PosicionActualFila;
        protected int PosicionActualColumna;

        protected Pieza(string nombre, Color color, int pActualFila, int pActualColumna)
        {
            NombrePieza = nombre;
            Color = color;
            PosicionActualFila = pActualFila;
            PosicionActualColumna = pActualColumna;
        }

        public abstract bool ValidarMovimiento(int posicionFinalFila, int posicionFinalColumna);



    }
}
