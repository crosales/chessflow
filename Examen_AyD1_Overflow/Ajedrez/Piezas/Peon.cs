﻿using System;

namespace Ajedrez
{
    public class Peon : Pieza
    {
        public bool PrimerTurno = false;
        public int Direccion;

        public Peon(string nombrePieza, Color color, int pActualFila, int pActualColumna, bool turno)
            : base(nombrePieza, color, pActualFila, pActualColumna)
        {

            PrimerTurno = turno;
            Direccion = 1;
        }


        public override bool ValidarMovimiento(int pFinalFila, int pFinalColumna)
        {

            if (Color.Equals(Color.Blanco))
                Direccion = -1;

            if ((pFinalFila - PosicionActualFila == Direccion) &&
                (pFinalColumna - PosicionActualColumna == 0))
            {

                PrimerTurno = false;
                return true;
            }

            if ((pFinalFila - PosicionActualFila == Direccion) &&
                (Math.Abs(pFinalColumna - PosicionActualColumna) == 1))
            {
                if (!Tablero.PeonPuedeComer(pFinalFila, pFinalColumna))
                {
                    Console.WriteLine("El movimiento no es valido, no está permitido");
                    return false;
                }

                PrimerTurno = false;
                return true;
            }

            if ((PrimerTurno && pFinalFila - PosicionActualFila == Direccion) &&
                (Math.Abs(pFinalColumna - PosicionActualColumna) == 2))
            {
                if (!Tablero.PeonPuedeComer(pFinalFila, pFinalColumna))
                {
                    Console.WriteLine("El movimiento no es valido, no está permitido");
                    return false;
                }

                PrimerTurno = false;
                return true;
            }
            else
            {
                Console.WriteLine("El movimiento no es valido, no está permitido");
            }
            return false;
        }

    }
}
