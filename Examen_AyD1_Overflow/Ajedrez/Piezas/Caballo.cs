﻿using System;


namespace Ajedrez
{
    public class Caballo:Pieza
    {

        public Caballo(string nombre, Color color, int pActualFila, int pActualColumna)
            : base(nombre, color, pActualFila, pActualColumna)
        {
        }

        public override bool ValidarMovimiento(int posicionFinalFila, int posicionFinalColumna)
        {
            if ((Math.Abs(posicionFinalFila - PosicionActualFila) == 2) && (Math.Abs(posicionFinalColumna - PosicionActualColumna) == 1) ||
            (Math.Abs(posicionFinalFila - PosicionActualFila) == 1) && (Math.Abs(posicionFinalColumna - PosicionActualColumna) == 2))
            {
                return true;
            }
            else
            {
                Console.WriteLine("El movimiento no es valido, no está permitido");
            }
            return false;
        }
    }
}
