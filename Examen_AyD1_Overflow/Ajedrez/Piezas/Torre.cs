using System;

namespace Ajedrez{

    public class Torre : Pieza {

        public Torre(string nombre, Color color, int pActualFila, int pActualColumna)
            : base(nombre, color, pActualFila, pActualColumna)
        {
            NombrePieza = nombre;
            Color = color;
            PosicionActualFila = pActualFila;
            PosicionActualColumna = pActualColumna;
        }

    
    public override bool ValidarMovimiento(int posicionFinalFila,int posicionFinalColumna){
        
        int vacia = 0;
        int ocupada = 0;

        if( (Math.Abs(posicionFinalFila - PosicionActualFila) > 0) && ((posicionFinalColumna - PosicionActualColumna) == 0) ||
            ((posicionFinalFila - PosicionActualFila) == 0) && (Math.Abs(posicionFinalColumna - PosicionActualColumna) > 0) )
        {
            int indice;
            int limite;
            if((posicionFinalColumna - PosicionActualColumna) == 0){ 
                if(posicionFinalFila - PosicionActualFila < 0){
                    indice = posicionFinalFila;
                    limite = PosicionActualFila;
                }    
                else{
                    indice = PosicionActualFila+1;
                    limite = posicionFinalFila+1;
                }    
                for(int fila=indice; fila<limite; fila++){
                    if(Tablero.Piezas[fila,posicionFinalColumna]==null || !(Tablero.Piezas[PosicionActualFila,PosicionActualColumna].Color.Equals(Tablero.Piezas[posicionFinalFila,posicionFinalColumna].Color)) )
                        vacia++;
                    else
                        ocupada++;
                }
                if (Math.Abs(posicionFinalFila - PosicionActualFila) == vacia)
                    return true;
                else
                {
                    if (!Principal.ValidandoJaqueMate)
                        Console.WriteLine("Movimiento invalido, no puede saltar piezas.");
                }
                return false;
            }

            if ((posicionFinalFila - PosicionActualFila) == 0)
            {
                if (posicionFinalColumna - PosicionActualColumna < 0)
                {
                    indice = posicionFinalColumna;
                    limite = PosicionActualColumna;
                }
                else
                {
                    indice = PosicionActualColumna + 1;
                    limite = posicionFinalColumna + 1;
                }
                for (int col = indice; col < limite; col++)
                {

                    if (Tablero.Piezas[posicionFinalFila, col] == null || !(Tablero.Piezas[PosicionActualFila, PosicionActualColumna].Color.Equals(Tablero.Piezas[posicionFinalFila, posicionFinalColumna].Color)))
                        vacia++;
                    else
                        ocupada++;
                }

                if (Math.Abs(posicionFinalColumna - PosicionActualColumna) == vacia)
                    return true;
                if (!Principal.ValidandoJaqueMate)
                    Console.WriteLine("Movimiento invalido, no puede saltar piezas.");
                return false;
            }
            
        }
        
        else{
            Console.WriteLine("El movimiento no es valido, no está permitido");
            return false;
        }
        return false;
    }
  }
}